/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mvc.modelo.dao;

import mvc.modelo.entidades.Cliente;

/**
 *
 * @author Grupo 2 Java
 */
public interface ClienteController {
    Cliente validate(String email, String password);

    public void crear(Cliente cliente);
}
