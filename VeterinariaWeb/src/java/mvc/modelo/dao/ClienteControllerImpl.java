/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mvc.modelo.dao;

import java.io.Serializable;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import mvc.modelo.entidades.Cliente;

/**
 *
 * @author Grupo 2 Java
 */
public class ClienteControllerImpl implements Serializable, ClienteController {

    public ClienteControllerImpl() {
        this.emf = Persistence.createEntityManagerFactory("PU");
    }
    
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    @Override
    public Cliente validate(String email, String password) {

        EntityManager em = getEntityManager();

        try {
            Query q = em.createNamedQuery("Cliente.validate");
            q.setParameter("email", email);
            q.setParameter("password", password);

            return (Cliente) q.getSingleResult();
        } finally {
            em.close();
        }
    }

    @Override
    public void crear(Cliente cliente) {
            
          EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(cliente);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
        
    }

}
