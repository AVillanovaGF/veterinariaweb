/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mvc.modelo.entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Grupo 2 Java
 */
@Entity
@Table(name = "animales")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Animal.findAll", query = "SELECT a FROM Animal a")
    , @NamedQuery(name = "Animal.findByAnimalid", query = "SELECT a FROM Animal a WHERE a.animalid = :animalid")
    , @NamedQuery(name = "Animal.findByNombre", query = "SELECT a FROM Animal a WHERE a.nombre = :nombre")
    , @NamedQuery(name = "Animal.findByFechaDeNacimiento", query = "SELECT a FROM Animal a WHERE a.fechaDeNacimiento = :fechaDeNacimiento")
    , @NamedQuery(name = "Animal.findByRaza", query = "SELECT a FROM Animal a WHERE a.raza = :raza")
    , @NamedQuery(name = "Animal.findBySexo", query = "SELECT a FROM Animal a WHERE a.sexo = :sexo")})
public class Animal implements Serializable {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "animalid")
    private List<Factura> facturaList;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Animal_id")
    private Integer animalid;
    @Basic(optional = false)
    @Column(name = "Nombre")
    private String nombre;
    @Basic(optional = false)
    @Column(name = "FechaDeNacimiento")
    @Temporal(TemporalType.DATE)
    private Date fechaDeNacimiento;
    @Column(name = "Raza")
    private String raza;
    @Basic(optional = false)
    @Column(name = "Sexo")
    private String sexo;
    @JoinColumn(name = "Cliente_id", referencedColumnName = "cliente_id")
    @ManyToOne(optional = false)
    private Cliente clienteid;

    public Animal() {
    }

    public Animal(Integer animalid) {
        this.animalid = animalid;
    }

    public Animal(Integer animalid, String nombre, Date fechaDeNacimiento,String raza, String sexo,Cliente cliente) {
        this.animalid = 1;
        this.nombre = nombre;
        this.fechaDeNacimiento = fechaDeNacimiento;
        this.raza=raza;
        this.sexo = sexo;
        this.clienteid= cliente;
    }

    public Integer getAnimalid() {
        return animalid;
    }

    public void setAnimalid(Integer animalid) {
        this.animalid = animalid;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFechaDeNacimiento() {
        return fechaDeNacimiento;
    }

    public void setFechaDeNacimiento(Date fechaDeNacimiento) {
        this.fechaDeNacimiento = fechaDeNacimiento;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public Cliente getClienteid() {
        return clienteid;
    }

    public void setClienteid(Cliente clienteid) {
        this.clienteid = clienteid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (animalid != null ? animalid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Animal)) {
            return false;
        }
        Animal other = (Animal) object;
        if ((this.animalid == null && other.animalid != null) || (this.animalid != null && !this.animalid.equals(other.animalid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mvc.modelo.entidades.Animal[ animalid=" + animalid + " ]";
    }

    public List<Factura> getFacturaList() {
        return facturaList;
    }

    public void setFacturaList(List<Factura> facturaList) {
        this.facturaList = facturaList;
    }
    
}
