/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mvc.modelo.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Grupo 2 Java
 */
@Entity
@Table(name = "facturas")
@NamedQueries({
    @NamedQuery(name = "Factura.findAll", query = "SELECT f FROM Factura f")
    , @NamedQuery(name = "Factura.findByFacturaid", query = "SELECT f FROM Factura f WHERE f.facturaid = :facturaid")
    , @NamedQuery(name = "Factura.findByTotal", query = "SELECT f FROM Factura f WHERE f.total = :total")})
public class Factura implements Serializable {

    @Basic(optional = false)
    @Column(name = "concepto")
    private String concepto;

  

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Factura_id")
    private Integer facturaid;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "total")
    private BigDecimal total;
    @JoinColumn(name = "Animal_id", referencedColumnName = "Animal_id")
    @ManyToOne(optional = false)
    private Animal animalid;
    @JoinColumn(name = "Cliente_id", referencedColumnName = "cliente_id")
    @ManyToOne(optional = false)
    private Cliente clienteid;

    public Factura() {
    }

    public Factura(Integer facturaid) {
        this.facturaid = facturaid;
    }

    public Factura(Integer facturaid, BigDecimal total) {
        this.facturaid = facturaid;
        this.total = total;
    }

    public Integer getFacturaid() {
        return facturaid;
    }

    public void setFacturaid(Integer facturaid) {
        this.facturaid = facturaid;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public Animal getAnimalid() {
        return animalid;
    }

    public void setAnimalid(Animal animalid) {
        this.animalid = animalid;
    }

    public Cliente getClienteid() {
        return clienteid;
    }

    public void setClienteid(Cliente clienteid) {
        this.clienteid = clienteid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (facturaid != null ? facturaid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Factura)) {
            return false;
        }
        Factura other = (Factura) object;
        if ((this.facturaid == null && other.facturaid != null) || (this.facturaid != null && !this.facturaid.equals(other.facturaid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mvc.dao.entidades.Factura[ facturaid=" + facturaid + " ]";
    }

    

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }
    
}
