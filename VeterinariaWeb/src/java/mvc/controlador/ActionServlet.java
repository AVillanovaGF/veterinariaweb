/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mvc.controlador;

import mvc.controlador.acciones.AltaAction;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mvc.controlador.acciones.AnimalesAction;
import mvc.controlador.acciones.BaseAction;
import mvc.controlador.acciones.DatosAction;
import mvc.controlador.acciones.DesconectarAction;
import mvc.controlador.acciones.FacturaAction;
import mvc.controlador.acciones.RegistrarAction;
import mvc.controlador.acciones.RegistrarAnimalAction;

import mvc.controlador.acciones.ValidarAction;

/**
 *
 * @author Grupo 2 Java
 */
@WebServlet(name = "ActionServlet", urlPatterns = {"*.do"})
public class ActionServlet extends HttpServlet {

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String patron_servlet = req.getServletPath();

        String accion = patron_servlet.substring(
                patron_servlet.indexOf("/") + 1,
                patron_servlet.lastIndexOf("."));

        String vista = "/login.jsp";

        BaseAction action = null;

        switch (accion) {
            case "validar":

                // Peticion validar.do
                action = new ValidarAction();
                break;
            case "alta":

                // Peticion validar.do
                action = new AltaAction();
                break;
            case "registrar":

                action = new RegistrarAction();
                break;
                
            case "registraAnimales":
                        System.out.println("a");

                action = new RegistrarAnimalAction();
                break;

            case "datos":
                // Peticion datos.do
                action = new DatosAction();
                break;
            case "animales":
                // Peticion datos.do
                
                action = new AnimalesAction();
                break;
            case "facturas":
                
                // Peticion datos.do
                action = new FacturaAction();
                break;

            case "desconectar":
                // Peticion desconectar.do
                action = new DesconectarAction();
                break;

        }

        // Ejecutar accion
        vista = action.route(req);

        // Reescribir la URL de la vista
        String vistaReescrita = resp.encodeURL(vista);

        // Objeto para reenvio asociado a la vista
        RequestDispatcher dispatcher = req.getRequestDispatcher(vistaReescrita);

        // Reenviar
        dispatcher.forward(req, resp);

    }

}
