/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mvc.controlador.acciones;

import javax.servlet.http.HttpServletRequest;
import mvc.controlador.acciones.BaseAction;

/**
 *
 * @author Ana7
 */
public class AltaAction extends BaseAction {

   
    @Override
    public String route(HttpServletRequest request) {
        
        return "/registro.jsp";
    }
    
}
