/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mvc.controlador.acciones;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import mvc.modelo.dao.AnimalController;
import mvc.modelo.dao.AnimalControllerImpl;
import mvc.modelo.dao.ClienteController;
import mvc.modelo.dao.ClienteControllerImpl;
import mvc.modelo.entidades.Animal;
import mvc.modelo.entidades.Cliente;

/**
 *
 * @author Grupo 2 Java
 */
public class RegistrarAnimalAction extends BaseAction{

    @Override
    public String route(HttpServletRequest request) {
        
       
        
        String nombre = request.getParameter("nombre");
        
        Date fechaDeNacimiento= new Date();
        try {
            fechaDeNacimiento = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("FechaDeNacimiento"));
        } catch (ParseException ex) {
            Logger.getLogger(RegistrarAnimalAction.class.getName()).log(Level.SEVERE, null, ex);
        }
                

        String raza = request.getParameter("raza");
        System.out.println(raza);
        String sexo = request.getParameter("sexo");
        
       Cliente cliente  = (Cliente) request.getSession().getAttribute("cliente");
              
              
     
        
        Animal animal = new Animal(1, nombre, fechaDeNacimiento, raza, sexo,cliente);
       
         AnimalController controller = new AnimalControllerImpl();
         
         controller.crear(animal);
         
        cliente.getAnimalList().add(animal);
        
        request.getSession().setAttribute("cliente", cliente);
        return "/animales.jsp";
    }
    
}
