package mvc.controlador.acciones;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class FacturaAction extends BaseAction {

    @Override
    public String route(HttpServletRequest request) {

        // Recuperar la sesion (usuario actual) SIN CREARLA en caso de
        // NO EXISTIR
        HttpSession session = request.getSession(false);

        // Comprobar si existía
        if ((session != null)&&( session.getAttribute("cliente"))!=null) {
            return "/facturas.jsp";
        } else {
            request.setAttribute("mensaje", "No ha inciado sesión");

            return "/login.jsp";
        }
    }
}
