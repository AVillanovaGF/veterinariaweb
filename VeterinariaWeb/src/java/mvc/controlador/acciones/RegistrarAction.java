/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mvc.controlador.acciones;

import javax.servlet.http.HttpServletRequest;
import mvc.modelo.dao.ClienteController;
import mvc.modelo.dao.ClienteControllerImpl;
import mvc.modelo.entidades.Cliente;

/**
 *
 * @author Grupo 2 Java
 */
public class RegistrarAction extends BaseAction{

    @Override
    public String route(HttpServletRequest request) {
        
        String nombre = request.getParameter("firstname");
        
        String apellidos = request.getParameter("lastname");
        
        String telefono = request.getParameter("telefono");

        String email = request.getParameter("email");

        String contrasena = request.getParameter("contrasena");
        
        String dni = request.getParameter("dni");
 
        // Validar
        if (nombre.isEmpty() || apellidos.isEmpty() || telefono.isEmpty() ||
                email.isEmpty() || contrasena.isEmpty() || dni.isEmpty()) {
            // Datos NO completados
            request.setAttribute("mensaje", "Campos obligatorioss");

            return "/registro.jsp";
        }
        Cliente cliente = new Cliente(1, nombre, apellidos, telefono, email, contrasena, dni);
        
         ClienteController controller = new ClienteControllerImpl();
         
         controller.crear(cliente);

        return "/login.jsp";
    }
    
}
