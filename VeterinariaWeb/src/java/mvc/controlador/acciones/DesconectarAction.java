package mvc.controlador.acciones;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class DesconectarAction extends BaseAction {

    @Override
    public String route(HttpServletRequest request) {
        
        // Recuperar la sesion (usuario actual) SIN CREARLA en caso de
        // NO EXISTIR
        HttpSession session = request.getSession(false);
        
        // Comprobar si existía
        if(session != null) {
            // Dar por finalizada la sesion
             request.setAttribute("mensaje", "Has cerrado tu sesión");
             
             session.invalidate();
            
            
        } else {
            request.setAttribute("mensaje", "Ha expirado tu sesión");
        }
        
        return "/login.jsp";
    }
    
}
