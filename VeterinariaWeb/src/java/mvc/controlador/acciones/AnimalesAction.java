package mvc.controlador.acciones;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class AnimalesAction extends BaseAction {

    @Override
    public String route(HttpServletRequest request) {

        // Recuperar la sesion (usuario actual) SIN CREARLA en caso de
        // NO EXISTIR
        HttpSession session = request.getSession(false);

        // Comprobar si existía
          if ((session != null)&&( session.getAttribute("cliente"))!=null) {
              
              
              
              
              
              
              
            return "/animales.jsp";
        } else {
            request.setAttribute("mensaje", "No ha iniciado sesión");

            return "/login.jsp";
        }
    }
}
