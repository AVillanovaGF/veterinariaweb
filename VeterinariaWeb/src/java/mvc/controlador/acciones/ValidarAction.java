package mvc.controlador.acciones;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import mvc.modelo.dao.ClienteController;
import mvc.modelo.dao.ClienteControllerImpl;
import mvc.modelo.entidades.Cliente;

public class ValidarAction extends BaseAction {

    @Override
    public String route(HttpServletRequest request) {

        // Recuperar los parametros de peticion
        String email = request.getParameter("email");

        String contrasena = request.getParameter("contrasena");
 
        // Validar
        if (email.isEmpty() || contrasena.isEmpty()) {
            // Datos NO completados
            request.setAttribute("mensaje", "Campos obligatorioss");

            return "/login.jsp";
        }

        // Cajas con contenido. Validar datos introducidos
        ClienteController controller = new ClienteControllerImpl();
        
        Cliente c = null;
        
        try {
            c = controller.validate(email, contrasena);
            System.out.println(c.toString());
            // Recuperar la sesion (usuario actual)
            HttpSession session = request.getSession();
            
            // Crear atributo de sesion con los datos del cliente que
            // se acaba de validar
            session.setAttribute("cliente", c);
            
        } catch(Exception ex) {
            request.setAttribute("mensaje", "Credenciales incorrectas");

            return "/login.jsp";
        
        }
        return "/inicial.jsp";
    }

}
