package mvc.controlador.acciones;

import javax.servlet.http.HttpServletRequest;

/*
    Clase base para todas las acciones de la aplicacion

    Permite una ejecucion polimorfica desde el service del Servlet
 */
public abstract class BaseAction {
    
    public abstract String route(HttpServletRequest request);
    
}
