<%-- 
    Document   : registro
    Created on : 08-abr-2018, 18:38:57
    Author     : Ana7
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> 
 <link href="resources/scripts/estilo.css" rel="stylesheet" type="text/css"/>
        
    </head>
    <body>
       
        <nav>
            <jsp:include page="menu.jsp" flush="true" />
        </nav>
        <section>
            <form  method="post" action='<%=response.encodeURL("registraAnimales.do")%>'    >
                <h1>Bienvenido al registro de Animales de CliniCan&#169</h1>
                <label>Nombre: </label>
                <input type="text" name="nombre" id="nombre" size="20"
                       maxlength="40" placeholder="Rex" required
                       value=""/>
                <br>
                <label>Fecha de Nacimiento </label>
                <input type="date" name="FechaDeNacimiento" id="FechaDeNacimiento" size="20"
                       maxlength="40" placeholder="12/12/1999" required
                       value=""/>
                <br>
                <label>Raza: </label>
                <input type="text" name="raza" id="raza" size="20"
                       maxlength="40" placeholder="desconocido" required
                       value=""/>
                <br>
                <label>Sexo: </label>
                <input type="text" name="sexo" id="sexo" size="20"
                       maxlength="40" placeholder="H" required
                       value=""/>
                <br>
                
                <input type="submit" value="   Enviar    "> 
            </form>
            <a href="animales.jsp">Volver</a>
        </section>

        <footer><%@include file="footer.jsp" %></footer>

    </body>
</html>
