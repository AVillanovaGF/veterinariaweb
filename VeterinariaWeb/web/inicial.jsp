<%-- 
    Document   : inicial.jsp
    Created on : 08-abr-2018, 18:03:03
    Author     : Ana7
--%>
<%--redireccionar a login si no hay usuario --%>
<% if(session.getAttribute("cliente")==null){   
%><jsp:forward page="login.jsp"></jsp:forward><%
}%>
<%@page import="mvc.modelo.entidades.Cliente"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Usuario</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> 
 <link href="resources/scripts/estilo.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        
        <nav>
            <jsp:include page="menu.jsp" flush="true" />
        </nav>
        <h1>${cliente.toString()}</h1>
        <h4>Telefono: <%=((Cliente) session.getAttribute("cliente")).getTelefono()%></h4>
        <h4>Email: <%=((Cliente) session.getAttribute("cliente")).getEmail()%></h4>
        <a href="desconectar.do"> Cerrar Sesion</a>
        <footer><%@include file="footer.jsp" %></footer>
    </body>
</html>
