
<nav class="navbar navbar-default">
  <div class="container-fluid">
   
    <ul class="nav navbar-nav">      
      <li><a href='<%=response.encodeURL("datos.do")%>'>Usuario</a></li>
      <li><a href='<%=response.encodeURL("animales.do")%>'>Mascotas</a></li>
      <li><a href='<%=response.encodeURL("facturas.do")%>'>Visitas a CliniCan&#169  </a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
        <li><a href='<%=response.encodeURL("desconectar.do")%>'><span class="glyphicon glyphicon-log-out"></span>Desconectar</a></li>
    </ul>
  </div>
</nav>
    
  