<% if (session.getAttribute("cliente") == null) {
%><jsp:forward page="login.jsp"></jsp:forward><%
    }%>

<%@page import="java.util.List"%>
<%@page import="mvc.modelo.entidades.Factura"%>
<%@page import="mvc.modelo.entidades.Cliente"%>
<%@page import="mvc.modelo.entidades.Animal"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Visitas CliniCan&#169</title>
       <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> 
 <link href="resources/scripts/estilo.css" rel="stylesheet" type="text/css"/>
    </head>
    
    <body>
       
        <nav>
            <jsp:include page="menu.jsp" flush="true" />
        </nav>

        <% if (session.getAttribute("cliente") != null) {
                Cliente cliente = (Cliente) session.getAttribute("cliente");
                List<Factura> lista= cliente.getFacturaList();
                
                if (!lista.isEmpty()) {

        %>
        <h1>Tus visistas a CliniCan&#169</h1>
        <table style="width:80%" border="1px red solid">
            <tr>
                <th>Animal</th>
                <th>Coste</th> 
                <th>Concepto</th>

            </tr>
            <%for (Factura f : lista) {%>
            <tr>
                <td>
                    <%= f.getAnimalid().getNombre()%>
                </td>
                <td>
                    <%= f.getTotal()%> €
                </td>
                <td>
                    <%= f.getConcepto()%>
                </td>
            </tr><%
                        }
                    } else {
%><h3>Vaya parece que aun no has traido tus animales</h3>
<br>
<h3>Quizas deberias hacernos una visita en CliniCan&#169</h3><%
                    }
                }
            %>
        </table>
 <a href="desconectar.do"> Cerrar Sesion</a>
        <footer><%@include file="footer.jsp" %></footer>
    </body>
</html>
