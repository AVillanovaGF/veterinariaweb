<% if (session.getAttribute("cliente") == null) {
%><jsp:forward page="login.jsp"></jsp:forward><%
    }%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.lang.Object"%>
<%@page import="mvc.modelo.entidades.Animal"%>
<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@page import="java.util.List"%>
<%@page import="mvc.modelo.entidades.Cliente"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Mascotas</title>
       
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> 
 <link href="resources/scripts/estilo.css" rel="stylesheet" type="text/css"/>
    </head>
    
    <body>
        
        <nav>
            <jsp:include page="menu.jsp" flush="true" />
        </nav>



        <% if (session.getAttribute("cliente") != null) {
                Cliente cliente = (Cliente) session.getAttribute("cliente");

                List<Animal> lista = cliente.getAnimalList();
                if (!lista.isEmpty()) {

        %>
        <h1>Tus animales:</h1>
        <table class="table table-hover">
            <tr class="success">
                <th>Nombre</th>
                <th>Fecha de nacimiento</th> 
                <th>Raza</th>
                <th>Sexo</th> 
            </tr>
            <%for (Animal a : lista) {%>
            <tr class="info">
                <td>
                    <%= a.getNombre()%>
                </td>
                <td>
                    <%= a.getFechaDeNacimiento()%>
                </td>
                <td>
                    <%= a.getRaza()%>
                </td>
                <td>
                    <%= a.getSexo()%>
                </td>
            </tr><%
                }
            %>
        </table>
        <a href="registroAnimales.jsp"> Registrar otro animal</a>
        <%
        } else {
        %>   
        <h1>Vaya, parece que no tienes ningun animal :C</h1>
        <a href="registroAnimales.jsp">Registrar un animal</a></br><%
                                }

                            } else {

                            }%>
        <a href="desconectar.do"> Cerrar Sesion</a>
        <footer><%@include file="footer.jsp" %></footer>
    </body>
</html>
