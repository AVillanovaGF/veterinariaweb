<%-- 
    Document   : login
    Created on : 07-abr-2018, 13:39:20
    Author     : Ana7
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login</title>
        <style>
            html{
                height: 100%;
            }
            header{
                border: 0px red solid;
            }
            header img{
                height: 350px;
                width: 100%;
            }
            body{
                background-color: #CCEEFF;
                font-size: 1.4em;
            }
            section{
                text-align: center;
                padding-bottom: 20px;
                margin-bottom: 10px;



            }
            footer{
                text-align: center;
                position: fixed;
                left:45%;
                bottom:0%;  
                color: black;
            }
            a{
                color: blue;
            }
            a:hover{
                font-size: 1.2em;
                color: black;
            }
        </style>
    </head>
    <body>
        <header>
            <%@include file="header.jsp" %>
        </header>

        <section>
            <% if (request.getAttribute("mensaje") != null) {
            %><h1><%=request.getAttribute("mensaje")%></h1>
            <%
                }%>

            <form  method="post" action='<%=response.encodeURL("validar.do")%>'    >
                <h1>Ingresa tus datos para iniciar sesion</h1>
                <label>Usuario: </label>
                <input type="text" name="email" id="email" size="20"
                       maxlength="40" placeholder="email" 
                       value="ejemplo@ejemplo.com"/>
                <br> 
                <label> Contraseña:</label>
                <input type="password" name="contrasena" id="contrasena" size="17"
                       maxlength="40" placeholder="contrasena" 
                       value="55555"/>
                <br>
                <input type="submit" value="   LOGIN    "> 
            </form>
            <a href="alta.do">¿No tienes cuenta? , click aqui</a>
        </section>

        <footer><%@include file="footer.jsp" %></footer>

    </body>
</html>
