<%-- 
    Document   : registro
    Created on : 08-abr-2018, 18:38:57
    Author     : Ana7
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> 
 <link href="resources/scripts/estilo.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        
        <nav>
            <jsp:include page="menu.jsp" flush="true" />
        </nav>
        <section>
            <form  method="post" action='<%=response.encodeURL("registrar.do")%>'    >
                <h1>Bienvenido al registro de usuarios de CliniCan&#169</h1>
                <label>Nombre: </label>
                <input type="text" name="firstname" id="firstname" size="20"
                       maxlength="40" placeholder="pedro" required
                       value=""/>
                <br>
                <label>Apellidos: </label>
                <input type="text" name="lastname" id="lastname" size="20"
                       maxlength="40" placeholder="gonzalez" required
                       value=""/>
                <br>
                <label>Telefono: </label>
                <input type="tel" name="telefono" id="telefono" size="20"
                       maxlength="40" placeholder="9194551" required
                       value=""/>
                <br>
                <label>Email: </label>
                <input type="text" name="email" id="email" size="20"
                       maxlength="40" placeholder="ejemplo@ejemplo.com" required
                       value=""/>
                <br>
                <label>Contraseña:</label>
                <input type="password" name="contrasena" id="contrasena" size="17"
                       maxlength="40" placeholder="contrasena" required
                       value=""/>
                <br>
                <label>Dni:</label>
                <input type="text" name="dni" id="contrasena" size="17"
                       maxlength="40" placeholder="5051651F" required
                       value=""/>
                <br>
                <input type="submit" value="   Enviar    "> 
            </form>
            <a href="login.jsp">Volver</a>
        </section>

        <footer><%@include file="footer.jsp" %></footer>

    </body>
</html>
